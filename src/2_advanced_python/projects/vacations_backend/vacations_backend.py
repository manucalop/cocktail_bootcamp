import json, yaml
from data_loaders import ImageLoader, NewsLoader, WeatherLoader

class Location:
    def __init__(self, name, data_root_path='./data/'):
        self.name = name
        self.data_path = data_root_path + self.name.lower() + '/'
        self.images = ImageLoader(name = self.name, root_path = self.data_path + 'img/')
        self.news = NewsLoader(name = self.name, root_path = self.data_path)
        self.weather = WeatherLoader(name = self.name, root_path = self.data_path)
        self.info = {'images': None,
                     'news': None,
                     'weather': None}

    def get_images(self, num_entries=5):
        self.images.pull_urls(num_entries)
        self.images.pull_files()
        self.info['images'] = self.images.data.to_dict()

    def get_news(self, num_entries=5):
        self.news.pull_data(num_entries)
        self.info['news'] = self.news.data.to_dict()

    def get_weather(self, num_entries=5):
        self.weather.pull_data(num_entries=num_entries)
        self.info['weather'] = self.weather.data.to_dict()

    def export_json(self):
        with open(self.data_path + self.name + ".json", "w") as outfile:
            json.dump(self.info, outfile, indent=4)

    def update_all(self, num_imgs : int, num_news : int, num_weather_days : int):
        self.get_images(num_imgs)
        self.get_news(num_news)
        self.get_weather(num_weather_days)
        self.export_json()
     

if __name__ == "__main__":
    print("Loading config file")
    with open('./config.yaml') as file:
        task = yaml.load(file, Loader=yaml.FullLoader)

    print("Building data structures for each location...")
    locations = [Location(i) for i in task['locations']]
    num_imgs = task['num_imgs']
    num_news = task['num_news']
    num_weather_days = task['num_weather_days']

    print("Pulling all requested data...")
    locations = list(map(lambda x : x.update_all(num_imgs, num_news, num_weather_days), locations))

    print("\nDone!")

