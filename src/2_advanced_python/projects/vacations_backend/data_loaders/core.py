import requests
import urllib
import wget
import os
import pandas as pd
from datetime import datetime
from abc import abstractmethod

unix2date = lambda x :  datetime.utcfromtimestamp(int(x))

class GoogleSearchApi:
    def __init__(self):
        self.headers = {
            'x-user-agent': "desktop",
            'x-proxy-location': "EU",
            'x-rapidapi-host': "google-search3.p.rapidapi.com",
            'x-rapidapi-key': "f6e1c29b57msh1aa2619c5859da3p1083c8jsn113ddd34a202"
        }
        self.base_url = 'https://rapidapi.p.rapidapi.com/api/v1/'
        self.valid_search_types = ['search','images','news','scholar','crawl']

    def query(self, string, search_type='search', num_entries=5):
        url = ''
        query = {
            "q": str(string),
            "num": num_entries
        }
        # Check if search_type is valid
        if search_type in self.valid_search_types:
            url = self.base_url + search_type + '/'
        else:
            print("Enter a valid type:")
            print(self.valid_search_types)
            return None
        resp = requests.get(url + urllib.parse.urlencode(query), headers=self.headers)
        return resp.json()

class OpenWeatherApi:
    def __init__(self):
        self.headers = {
            'x-user-agent': "desktop",
            'x-proxy-location': "EU",
            'x-rapidapi-host': "community-open-weather-map.p.rapidapi.com",
            'x-rapidapi-key': "f6e1c29b57msh1aa2619c5859da3p1083c8jsn113ddd34a202"
        }
        self.base_url = "https://community-open-weather-map.p.rapidapi.com/weather"
        self.valid_search_types = ['search','images','news','scholar','crawl']

    def query(self, string, num_entries=5):
        query = {
            "q": str(string),
            "units": 'metric',
        }
        response = requests.request("GET", self.base_url, headers=self.headers, params=query)
        return response.json()

    def get_weather_info(self,string):
        data = self.query(string)
        df = pd.DataFrame([],columns=['date', 'description', 'temperature', 'pressure', 'humidity', 'wind_speed', 'sunrise', 'Sunset'])
        df = pd.concat([df, pd.Series([unix2date(data['dt']).strftime('%Y-%m-%d %H:%M:%S'), 
                                       data['weather'][0]['description'], 
                                       data['main']['temp'], 
                                       data['main']['pressure'], 
                                       data['main']['humidity'], 
                                       data['wind']['speed'], 
                                       unix2date(data['sys']['sunrise']).strftime('%H:%M'), 
                                       unix2date(data['sys']['sunset']).strftime('%H:%M'), 
                                       ], 
                                       index = df.columns)], axis=1, ignore_index=True)             

        return df.T.dropna().set_index('date')

class DataLoader:
    def __init__(self, name : str, root_path : str):
        self.name = name
        self.root_path = root_path
        # Create root_path if it doesn't exist
        if not os.path.exists(self.root_path):
            os.makedirs(self.root_path)
    @abstractmethod
    def pull_data(self, num_entries : int) -> None:
        pass

class ImageLoader(DataLoader):
    def __init__(self, name : str, root_path : str):
        super().__init__(name, root_path)
        self.data = pd.DataFrame([], columns=['filename', 'url'])
        self.api = GoogleSearchApi()

    def set_filenames(self):
        self.data['filename'] = [self.root_path + 'image_' + str(i) + '.jpg' for i in range(len(self.data))]

    def pull_urls(self, num_entries):
        result = self.api.query(self.name, search_type='images', num_entries=num_entries)
        img_urls = []
        for item in result['image_results']:
            img_urls.append(item['image']['src'])
        self.data['url'] = img_urls 
        self.set_filenames()

    def pull_files(self):
        for index, row in self.data.iterrows():
            print(index)
            if os.path.exists(row['filename']):
                os.remove(row['filename']) # if exist, remove it directly
            wget.download(row['url'], out=row['filename'])

    def pull_data(self, num_entries : int) -> None:
        self.pull_urls(num_entries)
        self.pull_files()

class NewsLoader(DataLoader):
    def __init__(self, name : str, root_path : str):
        super().__init__(name, root_path)
        self.data = pd.DataFrame([], columns=['title', 'link'])
        self.api = GoogleSearchApi()
    def pull_data(self, num_entries : int) -> None:
        news = self.api.query(self.name, search_type='news', num_entries=num_entries)
        for entry in news['entries'][0:num_entries+1]:
            self.data = pd.concat([self.data, pd.Series({'title': entry['title'], 'link': entry['link']})], axis=1, ignore_index=True)             
        self.data = self.data.T.dropna().reset_index(drop=True)

class WeatherLoader(DataLoader):
    def __init__(self, name : str, root_path : str):
        super().__init__(name, root_path)
        self.data = pd.DataFrame([],columns=['date','description', 'temperature', 'pressure', 'humidity', 'wind_speed', 'sunrise', 'sunset'])
        self.api = OpenWeatherApi()
    def pull_data(self, num_entries : int) -> None:
        result = self.api.query(self.name)
        self.data = pd.concat([self.data, pd.Series([unix2date(result['dt']).strftime('%Y-%m-%d %H:%M:%S'), 
                                       result['weather'][0]['description'], 
                                       result['main']['temp'], 
                                       result['main']['pressure'], 
                                       result['main']['humidity'], 
                                       result['wind']['speed'], 
                                       unix2date(result['sys']['sunrise']).strftime('%H:%M'), 
                                       unix2date(result['sys']['sunset']).strftime('%H:%M'), 
                                       ], 
                                       index = self.data.columns)], axis=1, ignore_index=True)             

        self.data = self.data.T.dropna().set_index('date')
