from __future__ import print_function

import os.path
import pandas as pd
import smtplib, ssl
import time
import datetime

from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError


# If modifying these scopes, delete the file token.json.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

# The ID and range of a sample spreadsheet.
SAMPLE_SPREADSHEET_ID = '1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms'
SAMPLE_RANGE_NAME = 'Class Data!A2:E'

CLIENTES_POTENCIALES_ID = '12rowPjQXWy_TNms7uAqfaMBy0dyjR_CFg6gq_u4V7tY'
CLIENTES_POTENCIALES_RANGE = 'Hoja 1!A1:C5'
MENSAJES_CLIENTES_ID = '1JEouLKLB-quLhWYmawsVNgs3mMYGwjJojoJ77RDkGgM'
MENSAJES_CLIENTES_RANGE = 'Hoja 1!A1:C5'


class SpreadsheetApi:# {{{

    def __init__(self):# {{{
        self.creds = None
        if os.path.exists('token.json'):
            self.creds = Credentials.from_authorized_user_file('token.json', SCOPES)
        # If there are no (valid) credentials available, let the user log in.
        if not self.creds or not self.creds.valid:
            if self.creds and self.creds.expired and self.creds.refresh_token:
                self.creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', SCOPES)
                self.creds = flow.run_local_server(port=0)
            # Save the credentials for the next run
            with open('token.json', 'w') as token:
                token.write(self.creds.to_json())
        try:
            self.service = build('sheets', 'v4', credentials=self.creds)
        except HttpError as err:
            print(err)
# }}}

    def get_spreadsheet_values(self, spreadsheet_id, spreadsheet_range):# {{{
        sheet = self.service.spreadsheets()
        spreadsheet_dict = sheet.values().get(spreadsheetId=spreadsheet_id, range=spreadsheet_range).execute()
        spreadsheet_dict = spreadsheet_dict.get('values',[])
        # print(spreadsheet_dict)
#         values = result.get('values', [])
        return pd.DataFrame(spreadsheet_dict[1:],columns=spreadsheet_dict[0])
# }}}

# }}}

def send_email(receiver_email, # {{{
               message, 
               sender_email='bigfataco@gmail.com', 
               sender_pwd = 'untacogordo',
               smtp_server='smtp.gmail.com'):
    headers = {
            'Content-Type': 'text/html; charset=utf-8',
            'Content-Disposition': 'inline',
            'Content-Transfer-Encoding': '8bit',
            'From': sender_email,
            'To': receiver_email,
            'Date': datetime.datetime.now().strftime('%a, %d %b %Y  %H:%M:%S %Z'),
            'X-Mailer': 'python',
            'Subject': 'Manu Newsletter'
        }
    msg = ''
    for key, value in headers.items():
        msg += "%s: %s\n" % (key, value)
    msg += "\n%s\n"  % (message)

    context = ssl.create_default_context()
    port = 465 # For SSL
    with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
        server.login(sender_email, sender_pwd)
        server.sendmail(sender_email, receiver_email, msg.encode('utf8'))
# }}}

if __name__ == '__main__':
    sp_api = SpreadsheetApi()
    mensajes_clientes = sp_api.get_spreadsheet_values(MENSAJES_CLIENTES_ID, MENSAJES_CLIENTES_RANGE)
    clientes_potenciales = sp_api.get_spreadsheet_values(CLIENTES_POTENCIALES_ID, CLIENTES_POTENCIALES_RANGE)
    clientes_potenciales['Alta nueva'] = 1
    for _, row in clientes_potenciales.iterrows():
        message = mensajes_clientes[(mensajes_clientes['Tipo cliente'] == str(row['Cliente'])) & 
                                    (mensajes_clientes['Alta nueva'] == str(1))
                                   ]['Mensaje'].values[0]
        message = message.replace('NOMBRE_CLIENTE', row['Nombre cliente'])
        receiver_email = row['Mail']
        send_email(receiver_email=receiver_email, message = message)

