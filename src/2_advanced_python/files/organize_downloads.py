#!/usr/bin/python3
import os
cwd = os.path.expanduser("~/Downloads")
os.chdir(cwd)

dirs = ['Images', 'Documents', 'Software', 'Others']
print("Creating directories...")
for dir in dirs:
    if not os.path.exists(dir):
        os.mkdir(dir)
    else:
        print(dir, "already exists!")

files_in_cwd = [f for f in os.listdir(cwd) if os.path.isfile(os.path.join(cwd, f))]
print("Found these uncategorized files: ", files_in_cwd)
img_extensions = ['.jpg', '.jpeg', '.png', '.svg', '.gif', '.tif','.tiff']
doc_extensions = ['.doc', '.docx', '.pdf', '.xlsx', '.pptx']
sw_extensions = ['.sh', '.py', '.AppImage', '.deb', '.pkg', '.dmg']

for file in files_in_cwd:
    if file.endswith(tuple(img_extensions)):
         os.replace(file, dirs[0] + '/' + file)
    elif file.endswith(tuple(doc_extensions)):
         os.replace(file, dirs[1] + '/' + file)
    elif file.endswith(tuple(sw_extensions)):
         os.replace(file, dirs[2] + '/' + file)
    else:
         os.replace(file, dirs[3] + '/' + file)

print("Files have been organized!")


