import pandas as pd
import numpy as np
address = "https://raw.githubusercontent.com/justmarkham/DAT8/master/data/chipotle.tsv"
chipo = pd.read_csv(address, sep='\t')
print(chipo.head())
prices = chipo['item_price'].values
prices = np.array(list(map(lambda x : float(x[1:]), prices)))
chipo['item_price'] = prices
max_price = chipo.iloc[chipo['item_price'].idxmax()]
print(max_price['item_name'])

print("Step 4: ")
over_ten_items = chipo[chipo['item_price'] > 10]
print(len(over_ten_items.index))

print("Step 5: ")
print(over_ten_items[['item_name','item_price']])

print("Step 6: ")
print(over_ten_items.sort_values('item_name', ascending = True))

print("Step 7: ")
print(over_ten_items['item_price'].max())
print(over_ten_items.sort_values('item_price', ascending = False).iloc[0]['item_price'])

print("Step 8: ")
vsb = chipo[chipo['item_name'] == 'Veggie Salad Bowl']
print(len(vsb))

print("Step 9: ")
print(len(chipo[(chipo['item_name'] == 'Canned Soda') & (chipo['quantity'] > 1)]))

