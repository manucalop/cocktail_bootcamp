#!/usr/bin/python3
import pandas as pd
address = 'https://raw.githubusercontent.com/guipsamora/pandas_exercises/master/02_Filtering_%26_Sorting/Euro12/Euro_2012_stats_TEAM.csv'

euro12 = pd.read_csv(address)
print(euro12.head())

print("Step 4: ")
print(euro12['Goals'])
print("Step 5: ")
print(len(euro12))
print("Step 6: ")
print(len(euro12.columns))
print("Step 7: ")
discipline = euro12[['Team', 'Yellow Cards', 'Red Cards']]
print("Step 8: ")
print(discipline.sort_values('Red Cards', ascending=False))
print(discipline.sort_values('Yellow Cards', ascending=False))
print("Step 9: ")
print(discipline['Yellow Cards'].mean())
print("Step 10: ")
print(euro12[euro12['Goals']<6])
print("Step 11: ")
print(euro12[euro12['Team'].str.startswith('G')])
print("Step 12: ")
print(euro12.head(7))
print("Step 13: ")
print(euro12.iloc[:-3])
print("Step 14: ")
print(euro12[(euro12['Team'].isin(['England', 'Italy', 'Russia']))][['Team','% Goals-to-shots']])
