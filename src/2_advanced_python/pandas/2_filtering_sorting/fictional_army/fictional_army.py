import pandas as pd
# Create an example dataframe about a fictional army
raw_data = {'regiment': ['Nighthawks', 'Nighthawks', 'Nighthawks', 'Nighthawks', 'Dragoons', 'Dragoons', 'Dragoons', 'Dragoons', 'Scouts', 'Scouts', 'Scouts', 'Scouts'],
'company': ['1st', '1st', '2nd', '2nd', '1st', '1st', '2nd', '2nd','1st', '1st', '2nd', '2nd'],
'deaths': [523, 52, 25, 616, 43, 234, 523, 62, 62, 73, 37, 35],
'battles': [5, 42, 2, 2, 4, 7, 8, 3, 4, 7, 8, 9],
'size': [1045, 957, 1099, 1400, 1592, 1006, 987, 849, 973, 1005, 1099, 1523],
'veterans': [1, 5, 62, 26, 73, 37, 949, 48, 48, 435, 63, 345],
'readiness': [1, 2, 3, 3, 2, 1, 2, 3, 2, 1, 2, 3],
'armored': [1, 0, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1],
'deserters': [4, 24, 31, 2, 3, 4, 24, 31, 2, 3, 2, 3],
'origin': ['Arizona', 'California', 'Texas', 'Florida', 'Maine', 'Iowa', 'Alaska', 'Washington', 'Oregon', 'Wyoming', 'Louisana', 'Georgia']}

army = pd.DataFrame(raw_data)
print(army.head())

print("Step 4: ")
army.set_index('origin',inplace=True)
print(army.head())

print("Step 5: ")
print(army[['veterans']])
print("Step 6: ")
print(army[['veterans', 'deaths']])
print("Step 7: ")
print(list(army.columns))
print("Step 8: ")
print(army.loc[['Maine','Alaska']][['deaths','deserters']])
print("Step 9: ")
print(army.iloc[3:8, 3:7])
print("Step 10: ")
print(army.iloc[5:])
print("Step 11: ")
print(army.iloc[:5])
print("Step 12: ")
print(army.iloc[:,3:8])
print("Step 13: ")
print(army[army['deaths'] > 50] )
print("Step 14: ")
print(army[(army['deaths'] > 500) | (army['deaths'] < 50)] )
print("Step 15: ")
print(army[~(army['regiment'] == 'Dragoons')])
print("Step 16: ")
print(army[(army.index == 'Texas') | (army.index == 'Arizona')])
print("Step 17: ")
print(army[army.index == 'Arizona'].iloc[:,2])
print("Step 18: ")
print(army.iloc[:,2])
