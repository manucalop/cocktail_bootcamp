#!/usr/bin/python3
import pandas as pd
import numpy as np
address = 'https://raw.githubusercontent.com/guipsamora/pandas_exercises/master/04_Apply/Students_Alcohol_Consumption/student-mat.csv'
df = pd.read_csv(address)
print("Step 4: ")
df = df.loc[:,'school':'guardian']
print(df.head())
print("Step 5: ")
capitalize = lambda x : x.upper()
print("Step 6: ")
df[['Mjob', 'Fjob']].applymap(capitalize)
print(df.head())
print("Step 7: ")
print(df.tail())
print("Step 8: ")
df[['Mjob', 'Fjob']] = df[['Mjob', 'Fjob']].applymap(capitalize)
print(df.head())
print("Step 9: ")
def majority(dfr):
    dfr['legal_drinker'] =  list(dfr['age']>17)
    return dfr
print("Step 10: ")
cols = list(df.select_dtypes('number').columns)
df[cols] = df.select_dtypes('number').applymap(lambda x : x*10)
print(df.head())
