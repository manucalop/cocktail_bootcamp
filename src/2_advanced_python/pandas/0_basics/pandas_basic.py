#!/usr/bin/python3
print("Ejercicio 1: ")# {{{
import pandas as pd
print("Pandas version: ", pd.__version__)
# }}}

print("Ejercicio 2: ")# {{{
df = pd.read_csv('laliga.csv')
# }}}

print("Ejercicio 3: ")# {{{
print(df.head(4))
# }}}

print("Ejercicio 4: ")# {{{
print(df.values)
# }}}

print("Ejercicio 5: ")# {{{
print(df.columns)
# }}}

print("Ejercicio 6: ")# {{{
pob_dict = {
    'Madrid':    6685471,
    'Galicia':   2698764,
    'Murcia':    1494442,
    'Andalucia': 8446561
}
pob_data  = [6685471, 2698764, 1494442, 8446561]
pob_index = ['Madrid', 'Galicia', 'Murcia', 'Andalucia']

poblacion = pd.Series(pob_dict)
poblacion = pd.Series(data=pob_data, index=pob_index)
print(poblacion)
# }}}

print("Ejercicio 7: ")# {{{
print(poblacion.values)
print(poblacion.index)
# }}}

print("Ejercicio 8: ")# {{{
print(poblacion.iloc[0])
print(poblacion.loc['Galicia'])
print(poblacion.loc['Galicia':'Andalucia'])
print('Galicia' in poblacion.index)
print(poblacion[0])
print(poblacion['Galicia'])
print(poblacion['Galicia':'Andalucia'])
# }}}

print("Ejercicio 9: ")# {{{
pob_paro = [9.99, 11.74, 16.08, 20.8]
tasa_paro = pd.Series(data=pob_paro, index=pob_index)
# data = pd.DataFrame([poblacion, tasa_paro])
data = pd.DataFrame({'poblacion': poblacion, 'tasa_paro' : tasa_paro})
print(data)
# }}}

print("Ejercicio 10: ")# {{{
print(data['poblacion'], data['tasa_paro'])
# }}}

print("Ejercicio 11: ")# {{{
data2 = data
data2['superficies'] = [8028, 29575, 11314, 87599]
data2['superficies'] = pd.Series([8028, 29575, 11314, 87599], index=pob_index)
print(data2)
# }}}

print("Ejercicio 12: ")# {{{
data2['densidad'] = data2['poblacion'] / data2['superficies']
print(data2)
#}}}







