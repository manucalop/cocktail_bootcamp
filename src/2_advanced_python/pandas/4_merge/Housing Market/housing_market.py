import pandas as pd
import numpy as np
s1 = pd.Series(np.random.randint(1,4,size=100))
s2 = pd.Series(np.random.randint(1,3,size=100))
s3 = pd.Series(np.random.randint(int(10e3),int(30e3),size=100))

print("Step 3:")
df = pd.DataFrame({'s1':s1,'s2':s2,'s3':s3})
print(df)
print("Step 4:")
df = df.rename(columns={'s1':'bedrs','s2':'bathrs','s3':'price_sqr_meter'})
print(df)
print("Step 5:")
df['bigcolumn'] = s3
print(df)
print("Step 6:")
print(df.index[-1])
print("Step 7:")
print(df.reindex(index=range(0,300)))
