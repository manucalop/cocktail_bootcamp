#!/usr/bin/python3
import pandas as pd
import numpy as np

cars1 = pd.read_csv("https://raw.githubusercontent.com/guipsamora/pandas_exercises/master/05_Merge/Auto_MPG/cars1.csv")
cars2 = pd.read_csv("https://raw.githubusercontent.com/guipsamora/pandas_exercises/master/05_Merge/Auto_MPG/cars2.csv")

cars1 = cars1.loc[:,:'car']
print(cars1.head())
print(cars2.head())
print("Step 5: ")
print(len(cars1))
print(len(cars2))
print("Step 6: ")
cars = pd.concat([cars1, cars2])
print("Step 7: ")
owners = pd.Series( np.random.randint(15000, 73000, cars.shape[0]))
print("Step 8: ")
cars['owners'] =  owners.values

