#!/usr/bin/python3
import pandas as pd
food = pd.read_csv('food_100.csv', index_col=0)
print("Step 4")
print(food.head())
print("Step 5")
print(food.shape[0])
print("Step 6")
print(food.shape[1])
print("Step 7")
print(list(food.columns))
print("Step 8")
print(food.columns[104])
print("Step 9")
col = food.columns[104]
print(food[col].dtype)
print("Step 10")
print(food.index)
print("Step 11")
prod = food.iloc[19]
print(list(filter(lambda x: 'name' in x.lower(), prod.index)))
print(prod['product_name'])
