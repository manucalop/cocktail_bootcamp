import pandas as pd
users = pd.read_csv('https://raw.githubusercontent.com/justmarkham/DAT8/master/data/u.user', sep='|', index_col='user_id')
print("Step 4: ")
# print(users.iloc[:25])
print(users.head(25))

print("Step 5: ")
print(users.tail(10))

print("Step 6: ")
print(users.shape[0]*users.shape[1])
print(len(users.index)*len(users.columns))

print("Step 7: ")
print(len(users.columns))
print(users.shape[1])

print("Step 8: ")
print(list(users.columns))

print("Step 9: ")
print(list(users.index))

print("Step 10: ")
type(users['age'])
type(users['gender'])

print("Step 11: ")
print(users['occupation'])

print("Step 12: ")
print(len(users['occupation'].unique()))

print("Step 13: ")
print(users['occupation'].value_counts().head(1))

print("Step 14: ")
print(users.info())

print("Step 15: ")
print(users.describe(include='all'))

print("Step 16: ")
print(users['occupation'].describe())

print("Step 17: ")
print(users['age'].mean())

print("Step 18: ")
print(users['age'].value_counts().tail(1))









