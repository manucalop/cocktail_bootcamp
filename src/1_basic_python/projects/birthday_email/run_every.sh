#!/bin/sh
# Schedule a crontab job to run this script for every t minutes
t=$1
if [ -z "$t" ]
then
    echo "No time selected. Provide it as argument in minutes."
else
    # crontab -l > mycron
    crontab -r
    echo "$t * * * * cd $(pwd) && ./birthday_email.py" >> mycron
    crontab mycron
    rm mycron
fi

