# Send birthday email to your contacts

## Schedule a crontab job to execute the script every 30 mins
```
crontab -e

30 * * * * cd path_to/birthday_email && ./birthday_email.py
```

