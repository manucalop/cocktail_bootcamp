#!/Users/manuelcastillo/venvs/cocktail/bin/python
import smtplib, ssl
import time
import pandas as pd
import datetime

def send_email(receiver_email, 
               message, 
               sender_email='bigfataco@gmail.com', 
               sender_pwd = 'untacogordo',
               smtp_server='smtp.gmail.com'):
    context = ssl.create_default_context()
    port = 465 # For SSL
    with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
        server.login(sender_email, sender_pwd)
        server.sendmail(sender_email, receiver_email, message)

def generate_happy_birthday_message(name):
    message = """\
    Subject: Happy Birthday {}!

    Happy 31st Brithday {}!

    Manuel from Python.""".format(name,name)
    return message

def get_birthday_contacts_from_csv(csv_file):
    today = datetime.datetime.now().strftime("%d-%m")
    contacts= pd.read_csv(csv_file)
    return contacts[contacts["Birthday"] == today]

def send_birthday_emails():
    to_congratulate = get_birthday_contacts_from_csv("birthdays.csv")
    for _, row in to_congratulate.iterrows():
        message = generate_happy_birthday_message(row['Name'])
        receiver_email = row['email']
        send_email(receiver_email=receiver_email, message = message)

send_birthday_emails()
