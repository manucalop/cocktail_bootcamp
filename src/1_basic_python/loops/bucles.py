#!/usr/bin/python3

print("Ejercicio 1")# {{{
ejer_1 = ["VW", "Audi", "Renault", "VW", "BMW"]
ejer_1_sol = [i for i in ejer_1 if i !="VW"]
print(ejer_1_sol)
i = 0
ejer_1_sol = []
while i<len(ejer_1):
    if ejer_1[i] != "VW":
        ejer_1_sol.append(ejer_1[i])
    i += 1
print(ejer_1_sol)
# }}}

print("Ejercicio 2")# {{{
for i in range(10,-11,-1):
    print(i)
# }}}

print("Ejercicio 3")# {{{
nums = []
suma = 0
while True:
    nums.append(int(input('Give me a number:')))
    if nums[-1] == 0:
        break

print(nums, sum(nums))
# }}}

print("Ejercicio 4")# {{{
nums = []
suma = 0
while True:
    nums.append(int(input('Give me a number:')))
    if nums[-1] < 0:
        print("Numero negativo, por favor, introduzca un numero positivo")
        nums.pop()
    if nums[-1] == 0:
        break

print(nums, sum(nums))
# }}}

print("Ejercicio 5")# {{{
string = "Python"
print(string[-1:] + string[:-1])
# }}}

print("Ejercicio 6")# {{{
string = "En un lugar de La Mancha, de cuyo nombre no quiero acordarme"
print(string.lower().count("m") )

# }}}

print("Ejercicio 7")# {{{
letra = input("Dame letra: ")
string = letra
for i in range(5,0,-1):
    for j in range(i-1):
        string += " " + letra
    print(string)
    string = letra
# }}}

print("Ejercicio 8")# {{{
num = int(input("Dame num: "))
string = str(1)
for i in range(num,0,-1):
    for j in range(2,i+1):
        string += " " + str(j)
    print(string)
    string = str(1)

# }}}

print("Ejercicio 9")# {{{

# }}}

print("Ejercicio 10")# {{{
sequence = [i for i in range(1,11) if (i != 3 and i != 4 and i != 9)]
print(sequence)
sequence = []
for i in range(1,11):
    if (i == 3 or i == 4 or i == 9):
        continue
    sequence.append(i)
print(sequence)

# }}}

print("Ejercicio 11")# {{{
ingresos = [100, 200, 500, 100, 600] 
gastos = [50, 20, 70, 0, 25]
resultado = []
try:
    resultado = [i/j for i in ingresos for j in gastos]
    print(resultado)
except Exception as e:
    print(e)
# }}}

print("Ejercicio 12")# {{{
ingresos = [100, 200, 500, 100, 600] 
gastos = [50, 20, 70, 0, 25]
resultado = []
try:
    resultado = [i/j for i in ingresos for j in gastos]
    print(resultado)
except Exception as e:
    print(e)

# }}}
