#!/usr/bin/python3

print("Ejercicio 1")#{{{
def num2sem(num):
    if(num == 1):
        return "Monday"
    elif(num == 2):
        return "Tuesday"
    elif(num == 3):
        return "Wednesday"
    elif(num == 4):
        return "Thursday"
    elif(num == 5):
        return "Friday"
    elif(num == 6):
        return "Saturday"
    elif(num == 7):
        return "Sunday"
    else:
        print("Error. Num not in [1-7]")
        return 0

print(num2sem(3))
# }}}

print("Ejercicio 2")#{{{

def num2piramid(num):
    string = str(1)
    for i in range(num,0,-1):
        for j in range(2,i+1):
            string += " " + str(j)
        print(string)
        string = str(1)

num2piramid(9)
# }}}

print("Ejercicio 3")#{{{
def compare(num1,num2):
    if (num1 == num2):
        print("Equals")
    elif(num1 > num2):
        print("Bigger")
    else:
        print("Smaller")

compare(2,3)

# }}}

print("Ejercicio 4")#{{{

def letter_counter(string):
    return len(string)

print(letter_counter("Perita"))

# }}}

print("Ejercicio 5")#{{{

def letter_dict(string):
    mydict= {}
    for i in string:
        if i in mydict:
            mydict[i] +=1
        else:
            mydict[i] =1
    return mydict

print(letter_dict("Perrita"))

# }}}

print("Ejercicio 6")#{{{
def modifylist(lista, comando, elemento=None):
    if elemento == None:
        print("Put an element man!")
    else:
        if (comando.lower() == "add"):
            lista.append(elemento)
        elif (comando.lower() == "remove"):
            try:
                lista.remove(elemento)
            except:
                print("{} not in list {}".format(elemento, lista))
    return lista

print(modifylist([1,2,3,4,5], "REMOVE", 2))

# }}}

print("Ejercicio 7")#{{{
def sentence(*args):
    return ' '.join(args)
print(sentence('P', 'e', 'r', 'i', 't', 'a'))
# }}}

print("Ejercicio 8")#{{{

def fibonacci(n):
    if n <= 1:
        return n
    else:
        return(fibonacci(n-1) + fibonacci(n-2))

print(fibonacci(8))

# }}}

print("Ejercicio 9")#{{{
import math
square_area = lambda  x: x**2
triangle_area = lambda b,h: b*h/2
circle_area = lambda r: math.pi*r**2 

print(square_area(2))
print(triangle_area(2,3))
print(circle_area(2))

# }}}



