#!/usr/bin/python3

print("Ejercicio 1")# {{{
ejer_1 = [1,2,3,4,5]
ejer_1_inv = ejer_1[::-1]
print(ejer_1_inv)
# }}}

print("Ejercicio 2")# {{{
ejer2 = [i**2 for i in ejer_1]
print(ejer2)
# }}}

print("Ejercicio 3")# {{{
ejer_3_1 = ["Hola", "amigo"]
ejer_3_2 = ["Que", "tal"]
ejer_3 = []
for i in ejer_3_1:
    for j in ejer_3_2:
        ejer_3.append(i+" " +j)
print(ejer_3)
# }}}

print("Ejercicio 4")# {{{
ejer_4 = [20, 47, 19, 29, 45, 67, 78, 90]

for i,j in enumerate(ejer_4):
    if(j == 45):
        ejer_4[i]=0
print(ejer_4)
# }}}

print("Ejercicio 5")# {{{
ejer_5 = [3, 20, 3, 47, 19, 3, 29, 45, 67, 78, 90, 3, 3]
ejer_5_sol = [i for i in ejer_5 if i != 3 ]
print(ejer_5_sol)
# }}}

print("Ejercicio 6")# {{{
tupla = ("Code", 4, "Power" )
tupla_2 = ({'foo': 2})
print(type(tupla_2))
print(ejer_3[0], ejer_3[len(ejer_3)-1])
# }}}

print ("Ejercicio 7")# {{{
ejer_7 = ("cien", "cañones", "por", "banda")
ejer_7_sol = " ".join(ejer_7)
print(ejer_7_sol)
# }}}

print ("Ejercicio 8")# {{{
ejer_8 = (3, 20, 3, 47, 19, 3, 29, 45, 67, 78, 90, 3, 3)
tercer = ejer_8[2]
tercer_cola = ejer_8[-3]
print(tercer, tercer_cola)
# }}}

print ("Ejercicio 9")# {{{
ejer_9 = (3, 20, 3, 47, 19, 3, 29, 45, 67, 78, 90, 
          3, 3, 5, 2, 4, 7, 9, 4, 2, 4, 3, 3, 4, 6, 7)
num_3 = 0
for i in ejer_9:
    if i == 3:
        num_3 += 1
print("El 3 se repite {} veces".format(num_3))
tupla_nueva = tuple(list(ejer_9)[5:11])
print("La tupla nueva es: ", tupla_nueva)
print("La tupla ejer_9 tiene {} elementos".format(len(ejer_9)))
# }}}

print("Ejercicio 10")# {{{
print(60 in ejer_9)

# }}}

print("Ejercicio 11")# {{{
list_9 = list(ejer_9)
set_9 = set(ejer_9)
dict_9 = dict(enumerate(ejer_9))
print(dict_9)

# }}}

print("Ejercicio 12")# {{{
ejer_12 = [("x", 1), ("x", 2), ("x", 3), ("y", 1), ("y", 2), ("z", 1)]
print(dict(ejer_12))
# }}}

print("Ejercicio 13")# {{{
ejer_13 = {4:78, 2:98, 8:234, 5:29}
list_13_key_asc = sorted(ejer_13.keys())
list_13_value_desc = sorted(ejer_13.values(), reverse=True)
print(list_13_key_asc, list_13_value_desc)
ejer_13['new_key'] = 90
print(ejer_13)
print(list(ejer_13.keys())[1])
print("Printing all key, values:")
for key, value in ejer_13.items():
    print( key, value)
# }}}

print("Ejercicio 14")# {{{
ejer_14_1 = {1: 11, 2: 22}
ejer_14_2 = {3: 33, 4: 44}
ejer_14_1.update(ejer_14_2)
print(ejer_14_1)
# }}}

print("Ejercicio 15")# {{{
ejer_15 = {1: 11, 2: 22, 3: 33, 4: 44, 5: 55}
sum = 0
for i in list(ejer_15.values()):
    sum += i
print(sum)

# }}}

print("Ejercicio 16")# {{{
sum = 1
for i in list(ejer_15.values()):
    sum *= i
print(sum)

# }}}

print("Ejercicio 17")# {{{
set_17 = set([1,2,3])
set_17.add(4)
set_17.discard(4)
set_17.discard(10)
# }}}
