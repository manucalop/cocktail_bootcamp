#!/Users/manuelcastillo/venvs/cocktail/bin/python
import functools as ft

print("Ejercicio 1")# {{{
ejer_1 = ("to", "be", "or", "not", "to", "be", "that", "is", "the", "question")
print(ft.reduce(lambda x, y: x + ' ' + y, ejer_1))
# }}}

print("Ejercicio 2")# {{{
ejer_2 = (1,2,3,4,5)
print(list(map(lambda x : x**2, ejer_2)))
# }}}

print("Ejercicio 3")# {{{
ejer_3 = (1,2,3,4,5,15,21,22,33,34,35)
print(list(filter(lambda x: x%3 != 0 and x%5 != 0, ejer_3)))
# }}}

print("Ejercicio 4")# {{{
ejer_4 = (1000, 8000, 20000000, 40000000)
print(list(map(lambda x: x/166, ejer_4)))
# }}}

print("Ejercicio 5")# {{{
ejer_5 = ("Arya", "John", "Robb", "Bran", "Sansa", "Rickon")
print(list(map(lambda x : x.upper(), ejer_5)))
# }}}

print("Ejercicio 6")# {{{
ejer_6 = ("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo")
print(list(map(lambda x: ejer_6.index(x)+1, ejer_6)))
# }}}

print("Ejercicio 7")# {{{
ejer_7_1 = (2, 5, 8)
ejer_7_2 = (6, 3, 2)
print(list(map(lambda x, y : x*y, ejer_7_1, ejer_7_2)))
# }}}

print("Ejercicio 8")# {{{
ejer_8 = ("VW", "Audi", "Renault", "VW", "BMW")
print(tuple(filter(lambda x: x!="VW", ejer_8)))
# }}}

print("Ejercicio 9")# {{{
ejer_9 = ("2019-04-08", "2020-10-10", "2020-01-22", "2019-07-13", "2019-02-01")
print(tuple(filter(lambda x : '2020' in x, ejer_9)))
# }}}

print("Ejercicio 10")# {{{
ejer_10 = ("php", "w3r", "Python", "abcd", "Java", "aaa")
print(tuple(filter(lambda x : x[::-1] == x, ejer_10)))
# }}}


