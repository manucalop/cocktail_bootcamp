#!/Users/manuelcastillo/venvs/cocktail/bin/python
from aemet import Aemet

key = 'eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJtYW51Y2Fsb3BAZ21haWwuY29tIiwianRpIjoiNmZkZTk5ZmQtMGE5Zi00ZjVlLWE1ODctMmIwOTVlYWY0OWY2IiwiaXNzIjoiQUVNRVQiLCJpYXQiOjE2NDMyOTcyODEsInVzZXJJZCI6IjZmZGU5OWZkLTBhOWYtNGY1ZS1hNTg3LTJiMDk1ZWFmNDlmNiIsInJvbGUiOiIifQ.pmJGd8w0WXPbBM4Ksnp2beCmMBOtMEjRlGwJWEOQYiw'
aemet = Aemet(key,verbose=True)

print(aemet.descargar_mapa_analisis('mapa_analisis.jpg'))
print(aemet.descargar_mapa_rayos('mapa_rayos.jpg'))
print(aemet.descargar_mapa_riesgo_estimado_incendio('riesgo_estimado_incendio_canarias.jpg', area='c'))
print(aemet.descargar_mapa_satelite_nvdi('nvdi.jpg'))
print(aemet.get_prediccion_normalizada())
print(aemet.get_prediccion_normalizada(ambito='ccaa', ccaa='and'))
