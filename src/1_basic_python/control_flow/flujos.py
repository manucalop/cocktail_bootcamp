#!/usr/bin/python3

print("Ejercicio 1")# {{{
hora = 3
if (0 <= hora < 8):
    print("Durmiendo")
elif (9 <= hora < 18):
    print("Trabajando")
elif (19 <= hora < 21):
    print("Clase")
elif (22 <= hora < 24):
    print("Descanso")
else:
    print("Transporte o error")

#}}}

print("Ejercicio 2")# {{{
superficie = 120
distrito = "Centro"
if distrito in ["Moncloa", "Centro"] and superficie > 100:
    precio = 1000
elif distrito == "Salamanca" and superficie >= 150:
    precio = 1500
elif distrito == "Retiro" and 60 <= superficie <= 80:
    precio = 600
else:
    precio = 0
print(precio)
#}}}

print("Ejercicio 3")# {{{
if distrito == "Retiro":
    print("Distrito Retiro")
    if superficie > 100:
        print("Precio: ", 1000)
    else:
        print("Precio: ", 500)
else:
    print("Otro distrito")
#}}}

print("Ejercicio 4")# {{{
def ej_4(num, lista):
    for element in lista:
        print(element*num)
#}}}

print("Ejercicio 5")# {{{
l = [i for i in range(-10,0)]
print(l)
#}}}

print("Ejercicio 6")# {{{
list1 = [12, 15, 32, 42, 55, 75, 122, 132, 150, 180, 200]
div_5 = [i for i in list1 if i%5 == 0]
print(div_5)
#}}}

print("Ejercicio 7")# {{{
list1 = []
for i in range(151,350):
    if i%5 == 0 and i%7 == 0:
        list1.append(i)
print(list1)

#}}}

print("Ejercicio 8")# {{{
ne = 5
string = ""
for i in range(5,0,-1):
    for j in range(i,0,-1):
        string += str(j) + " "
    print(string)
    string = ""

#}}}

print("Ejercicio 9")# {{{
from random import randint
random_number = randint(1, 5)
n_vidas = 3
win = False
while True:
    guess = int(input("Guess your number: "))
    if guess == random_number:
        print("You win")
        break
    n_vidas -= 1
    if n_vidas == 0:
        print("You lost")
        break

#}}}

