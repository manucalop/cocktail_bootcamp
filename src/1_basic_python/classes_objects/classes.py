#!/usr/bin/python3

# Ejercicio 1{{{
class Tienda:
    """
    A class used to represent a Store.

    Attributes
    ----------
    nombre : str
    direccion: str
    n_empleados: int
    ventas: int

    Methods
    -------

    ventas_totales()
        Returns the sum of all sells
    ventas_medias_por_empleado()
        Returns the average sells by employee
    get_info()
        Returns Name and Direcction of the Store
    ventas_ultimo_mes()
        Returns last month's sales
    proyeccion(x : int)
        Returns last 3 month's projected sales given x marketing investment

    """
    tipo = 'Electrodomesticos'
    abierta = True
    def __init__(self, nombre, direccion, n_empleados, ventas):
        """
        Parameters
        ----------
        nombre : str
        direccion: str
        n_empleados: int
        ventas: int
        """
        self.nombre = nombre
        self.direccion = direccion
        self.n_empleados = n_empleados
        self.ventas = ventas
    def ventas_totales(self):
        return sum(self.ventas)
    def ventas_medias_por_empleado(self):
        return self.ventas_totales()/self.n_empleados
    def get_info(self):
        return "Nombre: " + self.nombre + ".\nDireccion: " + self.direccion
    def ventas_ultimo_mes(self):
        return self.ventas[-1]
    def proyeccion(self,x):
        """
        Returns last 3 month's projected sales given x marketing investment.
        If investment is less than 1000 multiply scale by 1.2. Otherwise scale by 1.5
        """
        if x < 1000:
            self.ventas = list(map(lambda x: x*1.2, self.ventas))
        else:
            self.ventas = list(map(lambda x: x*1.5, self.ventas))
        return self.ventas

tienda1 = Tienda("Electrolux","Calle Electro Latino 2", 3, [10, 20, 15])
tienda2 = Tienda("Bosch","Calle Like a Bosch 4", 5, [5, 12, 37])
tienda3 = Tienda("Miele","Avenida Miele a Perro 1", 4, [45, 10, 30])
print(tienda1.get_info())
print("Ventas totales: ", tienda1.ventas_totales())
print("Ventas medias por empleado: ", tienda1.ventas_medias_por_empleado())
print("Ventas ultimo mes: ", tienda1.ventas_ultimo_mes())
print("Proyeccion con 2k invertidos en marketing: ", tienda1.proyeccion(2000))


from functools import reduce
lista_tiendas = [tienda1, tienda2, tienda3]
ventas_ultimo_mes =  list(map(lambda x: x.ventas_ultimo_mes(), lista_tiendas ))
ventas_totales_ultimo_mes = reduce(lambda x, y : x+y, ventas_ultimo_mes)
print("Venta totales ultimo mes:", ventas_totales_ultimo_mes)
tiendas_avenida = list(filter(lambda x : "avenida" in x.direccion.lower(), lista_tiendas))
print("Tiendas en avenida: ", list(map(lambda x : x.nombre, tiendas_avenida)))


# }}}

# Ejercicio 2{{{
class Perro:
    patas = 4
    orejas = 2 
    ojos = 2 
    velocidad = 0
    def __init__(self, raza, pelo='Marron', owner = None):
        self.raza = raza
        self.pelo = pelo
        self.owner = owner
    def andar(self, aumento_velocidad):
        self.velocidad += aumento_velocidad
    def parar(self):
        self.velocidad = 0
    def ladrar(self, entrada):
        return "GUAU! " + entrada

# }}}

# Ejercicio 3{{{
class DecathlonStore:
    def __init__(self, tipo, direccion, n_empleados, ventas, gastos):
        self.tipo = tipo
        self.direccion = direccion
        self.n_empleados = n_empleados
        self.ventas = ventas
        self.gastos = gastos
        if ("city" in self.tipo.lower()):
            self.gastos = list(map(lambda x: x*1.1, self.gastos))

# }}}






